import {Component, HostBinding, OnInit} from '@angular/core';
import {CHANNELS} from '../../../../../channels';
import {ElectronService} from '../../../providers/electron.service';
import {Product} from '../../../data';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {PogressModalComponent} from '../../pogress-modal/pogress-modal.component';

@Component({
    selector: 'app-sales',
    templateUrl: './sales.component.html',
    styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {
    @HostBinding('class.flex-grow-1')
    // @HostBinding('class.flex')
    // @HostBinding('class.flex-column')

    public inventory: any;
    public new_sale_item = {} as Product;

    sales = [];

    myForm: FormGroup;
    total: number;

    constructor(private electronService: ElectronService,
                private formBuilder: FormBuilder,
                public dialog: MatDialog) {

    }

    ngOnInit() {

        this.myForm = this.formBuilder.group({
            item: [null, [
                Validators.required
            ]],
            quantity: [1, [
                Validators.required
            ]]
        });

        this.myForm.controls['quantity'].disable();
        this.myForm.controls['item'].valueChanges.subscribe(value => {
            console.log(this.myForm);
            if (value == null) {
                this.myForm.controls['quantity'].disable();
                this.myForm.controls['quantity'].setValue(0);
            } else {
                this.myForm.controls['quantity'].enable();
                this.myForm.controls['quantity'].setValidators([
                    Validators.min(1),
                    Validators.max(this.myForm.value.item.quantity)
                ]);
            }

            // this.myForm.controls['item'].valueChanges
        });

        this.getInventory();


    }


    getInventory() {
        this.electronService
            .ipcPromise(CHANNELS.products.find, {})
            .then((res: any[]) => {
                this.inventory = res.filter(value => value.quantity > 0);
                this.myForm.controls['item'].setValue(this.inventory[0]);
            })
            .catch(err => {
                alert('An error occurred while getting Inventory');
            });
    }

    add($element) {
        this.sales.push(this.myForm.value);
        // this.inventory = this.inventory.remove(this.myForm.value.item);
        const index = this.inventory.indexOf(this.myForm.value.item, 0);
        this.inventory.splice(index, 1);

        $element.focus();
        // this.total = 0;
        this.calc();

        // this.getInventory();

        // console.log(this.inventory)

        this.myForm.reset();
        this.myForm.controls['quantity'].setValue(1);
        this.myForm.controls['item'].setValue(this.inventory[0]);

    }

    revert($index) {

        const anies = this.sales.splice($index, 1);

        if (!(anies.length > 0)) {
            return;
        }

        const _anies = anies[0].item;

        this.inventory.push(anies[0].item);

        this.myForm.controls['item'].setValue(this.inventory[this.inventory.length - 1]);
        this.myForm.controls['quantity'].setValue(anies[0].quantity);


        this.calc();


    }


    calc() {
        this.total = 0;
        this.sales.forEach(i => {
            this.total += (i.item.unit_price * i.quantity);
        });
    }


    save() {

        const matDialogRef = this.dialog.open(PogressModalComponent, {
            data: {
                sales: this.sales
            },
            width: '60%'
        });

        matDialogRef.afterClosed().subscribe((v) => {
            if (v == null) {
                // if something goes wrong
            } else {
                this.sales = [];
                this.total = null;
                this.getInventory();
            }
        });


    }


}
