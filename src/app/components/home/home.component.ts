import {Component, OnInit} from '@angular/core';
import {ElectronService} from '../../providers/electron.service';
import {CHANNELS} from '../../../../channels';
import {Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


    public data = {
        some_data: 'data'
    };

    constructor(
        public electronService: ElectronService,
        private router: Router,
    ) {

    }

    ngOnInit() {
    }

    getUsers() {
        this.electronService
            .ipcPromise(CHANNELS.get_users, {})
            .then(function (data) {
                console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
    }


}
