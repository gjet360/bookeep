import {Component, OnInit} from '@angular/core';
import {Product} from '../../../data';
import {ElectronService} from '../../../providers/electron.service';
import {CHANNELS} from '../../../../../channels';
import {eraseStyles} from '@angular/animations/browser/src/util';

@Component({
    selector: 'app-inventory',
    templateUrl: './inventory.component.html',
    styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

    public new_prod = {} as Product;
    public inventory: any;

    constructor(public electronService: ElectronService) {
    }

    ngOnInit() {
        this.getInventory();
    }

    save() {
        // console.log(this.new_prod)

        this.electronService
            .ipcPromise(CHANNELS.products.add, this.new_prod)
            .then(res => {
                this.new_prod = {} as Product;
                this.getInventory();
            })
            .catch(err => {
                alert('Error while adding new product');
            });
    }

    update() {
        this.electronService
            .ipcPromise(CHANNELS.products.update, (this.new_prod as any)._id, this.new_prod)
            .then(() => {
                alert('Update successfull');
                this.getInventory();
                this.new_prod = {} as Product;
            })
            .catch(err => {
                alert('Error occoured');
            });
    }


    getInventory() {
        this.electronService
            .ipcPromise(CHANNELS.products.find, {})
            .then(res => {
                this.inventory = res;
            })
            .catch(err => {
                alert('An error occurred while getting Inventory');
            });
    }

    select(prod) {
        this.new_prod = prod;
    }


}

