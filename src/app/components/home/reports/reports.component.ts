import {Component, OnInit} from '@angular/core';
import {ElectronService} from '../../../providers/electron.service';
import {CHANNELS} from '../../../../../channels';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

    datefrom = new Date();
    dateto = new Date();

    purchases: any;

    constructor(private electronService: ElectronService) {
    }

    ngOnInit() {
        this.evaluate();
    }


    getReports() {
        this.electronService.ipcPromise(CHANNELS.purchases.find, {
            $and: [
                {
                    date: {$gte: this.datefrom.getTime()}

                },
                {
                    date: {$lte: this.dateto.getTime()}

                },
            ]
        })
            .then(values => {
                console.log(values);
                this.purchases = values;
            }).catch(err => {
            alert('err');
        });
    }


    evaluate() {
        // console.log('eval');
        this.datefrom.setHours(0, 0, 0);
        this.dateto.setHours(23, 59, 59);
        if (this.dateto < this.datefrom) {
            this.datefrom = this.dateto;
            this.datefrom.setHours(0, 0, 0);
            this.dateto.setHours(23, 59, 59);
        }


        this.getReports();
    }

    getTotal(products: any[]) {
        let total = 0;
        products.forEach(item => {
            total += item.item.quantity * item.quantity;
        });
        return total;
    }

}
