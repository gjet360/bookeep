import 'reflect-metadata';
import 'zone.js/dist/zone-mix';
import '../polyfills';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {HttpClientModule, HttpClient} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';

// NG Translate
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {ElectronService} from './providers/electron.service';

import {WebviewDirective} from './directives/webview.directive';

import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/home/login/login.component';
import {SalesComponent} from './components/home/sales/sales.component';
import {InventoryComponent} from './components/home/inventory/inventory.component';
import {ReportsComponent} from './components/home/reports/reports.component';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FilterPipe} from './providers/filter.pipe';
import {NavComponent} from './components/home/nav/nav.component';
import {PogressModalComponent} from './components/pogress-modal/pogress-modal.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        WebviewDirective,
        LoginComponent,
        SalesComponent,
        InventoryComponent,
        ReportsComponent,
        FilterPipe,
        NavComponent,
        PogressModalComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatDialogModule,
        MatButtonModule,
        MatDatepickerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpLoaderFactory),
                deps: [HttpClient]
            }
        })
    ],
    providers: [ElectronService, MatDatepickerModule],
    bootstrap: [AppComponent],
    entryComponents: [
        PogressModalComponent
    ]
})
export class AppModule {
}
