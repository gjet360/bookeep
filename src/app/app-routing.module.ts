import {HomeComponent} from './components/home/home.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/home/login/login.component';
import {SalesComponent} from './components/home/sales/sales.component';
import {InventoryComponent} from './components/home/inventory/inventory.component';
import {ReportsComponent} from './components/home/reports/reports.component';

const routes: Routes = [
        {
            path: '',
            pathMatch: 'full',
            redirectTo: 'sales'
        },
        {
            path: 'home',
            component: HomeComponent,
            children: [
                {
                    path: '',
                    pathMatch: 'full',
                    redirectTo: 'sales'
                },
                {
                    path: 'sales',
                    component: SalesComponent
                },
                {
                    path: 'inventory',
                    component: InventoryComponent
                },
                {
                    path: 'reports',
                    component: ReportsComponent
                }

            ]
        },
        {
            path: 'sales',
            component: SalesComponent
        },
        {
            path: 'inventory',
            component: InventoryComponent
        },
        {
            path: 'reports',
            component: ReportsComponent
        },
        {
            path: 'login',
            component: LoginComponent
        }

    ]
;

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
