export const CHANNELS = {
    database: 'db',
    get_users: 'get_users',
    products: {
        add: 'add_products',
        find: 'find_products',
        update: 'update_product'
    },
    purchases: {
        buy: 'buy_purchases',
        count: 'count_purchases',
        find: 'find_purchases'
    }
};
