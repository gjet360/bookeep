import {ipcMain} from 'electron';
// import Nedb from 'nedb';


import {CHANNELS} from './channels';
import * as Nedb from 'nedb';


const collection = {
    users: 'users',
    products: 'products',
    sales: 'sales'
};

export class DB {
    private users;
    private products;
    private sales;


    constructor(db = 'filename.db', products = 'products.db', sales = 'sales.db') {

        this.products = new Nedb({
            filename: '../data/' + products,
            autoload: true
        });

        this.users = new Nedb({
            filename: '../data/' + db,
            autoload: true
        });

        this.sales = new Nedb({
            filename: '../data/' + sales,
            autoload: true
        });

        this.users.insert({
            mydoc: 'a value'
        }, function (err, doc) {
            console.log(doc);
            console.error(err);
        });

        // this.users.addToIndexes()

        this.configureListeners();


    }


    private configureListeners() {
        ipcMain.on(CHANNELS.get_users, (evt, args) => {
            this.users.find({}, (err, docs) => {
                const data: IPCResponse = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };

                evt.returnValue = data;

            });
        });


        // products
        ipcMain.on(CHANNELS.products.add, (evt, args) => {
            console.log(args);
            this.products.insert(args[0], (err, docs) => {
                const data: IPCResponse = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };

                console.log(data);

                evt.returnValue = data;

            });
        });

        ipcMain.on(CHANNELS.products.find, (evt, args) => {
            console.log(args[0]);
            this.products.find(args[0])
                .sort({name: -1})
                .exec((err, docs) => {
                    const data: IPCResponse = {
                        success: (err !== undefined) || (err !== null),
                        error: err,
                        data: docs
                    };

                    // console.log(data);

                    evt.returnValue = data;

                });
        });

        ipcMain.on(CHANNELS.products.update, (evt, args) => {

            this.products.update({_id: args[0]}, args[1], {}, (err, docs) => {
                const data: IPCResponse = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };

                console.log(data);

                evt.returnValue = data;

            });
        });


        ipcMain.on(CHANNELS.purchases.buy, (evt, args) => {

            // (0) products, (1) name, (2) amount paid (3) receipt id

            const data: IPCResponse = {
                success: true,
                data: null,
                error: null
            };

            this.purchase(args[0])
                .then(values => {
                    data.data = values;

                    this.sales.insert({
                        products: args[0],
                        name: args[1],
                        amount_paid: args[2],
                        receipt: args[3],
                        date: new Date().getTime()
                    }, (err, res) => {
                        if (err) {
                            data.success = false;
                            data.error = new Error('error saving new salse');
                        } else {
                            data.data = res;
                        }

                        evt.returnValue = data;

                    });

                })
                .catch(err => {
                    console.log(err);

                    data.success = false;
                    data.error = err;

                    evt.returnValue = data;

                });


        });

        ipcMain.on(CHANNELS.purchases.count, (evt, args) => {

            this.sales.count(args[0], (err, res) => {
                const data: IPCResponse = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: res
                };

                evt.returnValue = data;

            });

        });

        ipcMain.on(CHANNELS.purchases.find, (evt, args) => {
            this.sales.find(args[0])
                .sort({date: -1})
                .exec((err, docs) => {
                    const data: IPCResponse = {
                        data: docs,
                        success: (err === null),
                        error: err
                    };

                    evt.returnValue = data;

                });
        });

    }


    response() {

    }

    purchase(data: any[]) {

        // console.log(datdata);

        const transaction = [];
        data.forEach(datum => {
            console.log(datum);
            transaction.push(this.single_purchase(datum.item, datum.quantity));
        });


        return Promise.all(transaction);
    }

    single_purchase({_id, name}, quantity, rollback: boolean = false): Promise<any> {
        return new Promise(((resolve, reject) => {

            this.products.update({
                    _id: _id
                }, {
                    $inc: {
                        quantity: rollback ? Math.abs(quantity) : Math.abs(quantity) * -1     // rollback: =  add
                    }
                },
                {}
                , (err, num) => {
                    if (err) {
                        reject(new Error('Unexpected transaction error'));
                    } else {
                        resolve(num);
                    }
                });
        }));
    }
}


// export class TransactionError extends Error {
//     constructor(message: string, public data: any) {
//         super(message);
//     }
// }


interface IPCResponse {
    success: boolean;
    data: any;
    error: any;
}

