"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
// import Nedb from 'nedb';
var channels_1 = require("./channels");
var Nedb = require("nedb");
var collection = {
    users: 'users',
    products: 'products',
    sales: 'sales'
};
var DB = /** @class */ (function () {
    function DB(db, products, sales) {
        if (db === void 0) { db = 'filename.db'; }
        if (products === void 0) { products = 'products.db'; }
        if (sales === void 0) { sales = 'sales.db'; }
        this.products = new Nedb({
            filename: '../data/' + products,
            autoload: true
        });
        this.users = new Nedb({
            filename: '../data/' + db,
            autoload: true
        });
        this.sales = new Nedb({
            filename: '../data/' + sales,
            autoload: true
        });
        this.users.insert({
            mydoc: 'a value'
        }, function (err, doc) {
            console.log(doc);
            console.error(err);
        });
        // this.users.addToIndexes()
        this.configureListeners();
    }
    DB.prototype.configureListeners = function () {
        var _this = this;
        electron_1.ipcMain.on(channels_1.CHANNELS.get_users, function (evt, args) {
            _this.users.find({}, function (err, docs) {
                var data = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };
                evt.returnValue = data;
            });
        });
        // products
        electron_1.ipcMain.on(channels_1.CHANNELS.products.add, function (evt, args) {
            console.log(args);
            _this.products.insert(args[0], function (err, docs) {
                var data = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };
                console.log(data);
                evt.returnValue = data;
            });
        });
        electron_1.ipcMain.on(channels_1.CHANNELS.products.find, function (evt, args) {
            console.log(args[0]);
            _this.products.find(args[0])
                .sort({ name: -1 })
                .exec(function (err, docs) {
                var data = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };
                // console.log(data);
                evt.returnValue = data;
            });
        });
        electron_1.ipcMain.on(channels_1.CHANNELS.products.update, function (evt, args) {
            _this.products.update({ _id: args[0] }, args[1], {}, function (err, docs) {
                var data = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: docs
                };
                console.log(data);
                evt.returnValue = data;
            });
        });
        electron_1.ipcMain.on(channels_1.CHANNELS.purchases.buy, function (evt, args) {
            // (0) products, (1) name, (2) amount paid (3) receipt id
            var data = {
                success: true,
                data: null,
                error: null
            };
            _this.purchase(args[0])
                .then(function (values) {
                data.data = values;
                _this.sales.insert({
                    products: args[0],
                    name: args[1],
                    amount_paid: args[2],
                    receipt: args[3],
                    date: new Date().getTime()
                }, function (err, res) {
                    if (err) {
                        data.success = false;
                        data.error = new Error('error saving new salse');
                    }
                    else {
                        data.data = res;
                    }
                    evt.returnValue = data;
                });
            })
                .catch(function (err) {
                console.log(err);
                data.success = false;
                data.error = err;
                evt.returnValue = data;
            });
        });
        electron_1.ipcMain.on(channels_1.CHANNELS.purchases.count, function (evt, args) {
            _this.sales.count(args[0], function (err, res) {
                var data = {
                    success: (err !== undefined) || (err !== null),
                    error: err,
                    data: res
                };
                evt.returnValue = data;
            });
        });
        electron_1.ipcMain.on(channels_1.CHANNELS.purchases.find, function (evt, args) {
            _this.sales.find(args[0])
                .sort({ date: -1 })
                .exec(function (err, docs) {
                var data = {
                    data: docs,
                    success: (err === null),
                    error: err
                };
                evt.returnValue = data;
            });
        });
    };
    DB.prototype.response = function () {
    };
    DB.prototype.purchase = function (data) {
        // console.log(datdata);
        var _this = this;
        var transaction = [];
        data.forEach(function (datum) {
            console.log(datum);
            transaction.push(_this.single_purchase(datum.item, datum.quantity));
        });
        return Promise.all(transaction);
    };
    DB.prototype.single_purchase = function (_a, quantity, rollback) {
        var _this = this;
        var _id = _a._id, name = _a.name;
        if (rollback === void 0) { rollback = false; }
        return new Promise((function (resolve, reject) {
            _this.products.update({
                _id: _id
            }, {
                $inc: {
                    quantity: rollback ? Math.abs(quantity) : Math.abs(quantity) * -1 // rollback: =  add
                }
            }, {}, function (err, num) {
                if (err) {
                    reject(new Error('Unexpected transaction error'));
                }
                else {
                    resolve(num);
                }
            });
        }));
    };
    return DB;
}());
exports.DB = DB;
//# sourceMappingURL=database.js.map